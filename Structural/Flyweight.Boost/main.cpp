#include <iostream>
#include <vector>
#include <boost/flyweight.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

struct FirstNamePool {};
struct LastNamePool {};

class Platnik
{
    int id_;
    boost::flyweight<std::string> imie_;
    std::string nazwisko_;
public:
    Platnik(int id, const string& imie, const string& nazwisko)
        : id_(id), imie_(imie), nazwisko_(nazwisko)
    {}

    int id() const
    {
        return id_;
    }

    string imie() const
    {
        return imie_;
    }

    void set_imie(const string& imie)
    {
        imie_ = imie;
    }

//    void to_upper()
//    {
//        imie_ = boost::to_upper_copy(imie_.get());
//        boost::to_upper(nazwisko_);
//    }

    string nazwisko() const
    {
        return nazwisko_;
    }

    void set_nazwisko(const string& nazwisko)
    {
        nazwisko_ = nazwisko;
    }

    bool operator==(const Platnik& p)
    {
        return id_ == p.id_;
    }
};


int main()
{
    vector<Platnik> platnicy;
    platnicy.push_back(Platnik(1, "Jan", "Kowalski"));
    platnicy.push_back(Platnik(2, "Jan", "Nowak"));
    platnicy.push_back(Platnik(3, "Anna", "Kowalska"));
    platnicy.push_back(Platnik(1, "Anna", "Nowakowska"));

    platnicy[0].to_upper();


    for(auto& p : platnicy)
    {
        cout << p.imie() << " " << p.nazwisko() << endl;
    }
}

