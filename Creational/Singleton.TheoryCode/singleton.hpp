#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>
#include <memory>
#include <mutex>
#include <atomic>

class Singleton
{
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

	static Singleton& instance()
	{
        std::call_once(is_initialized_, [=] { instance_.reset(new Singleton()); });

        return *instance_;
	}

	void do_something();

    ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
    {
        std::cout << "Singleton has been destroyed!" << std::endl;
    }

private:
    static std::once_flag is_initialized_;
    static std::unique_ptr<Singleton> instance_;

	Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
	{ 
		std::cout << "Constructor of singleton" << std::endl; 
    }
};

std::once_flag Singleton::is_initialized_;
std::unique_ptr<Singleton> Singleton::instance_ {nullptr};

void Singleton::do_something()
{
	std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}

#endif /*SINGLETON_HPP_*/
