#ifndef SQUARE_HPP_
#define SQUARE_HPP_

// TODO: Dodać klase Square

#include <iostream>
#include "shape.hpp"

namespace Drawing
{

class Square : public ShapeBase
{
private:
    unsigned int size_;
public:
    Square(int x = 0, int y = 0, size_t size = 0) : ShapeBase(x, y), size_(size)
    {
    }

    unsigned int size() const
    {
        return size_;
    }

    void set_size(unsigned int size)
    {
        size_ = size;
    }

    virtual void draw() const
    {
        std::cout << "Drawing a square at: " << point() << " size: " << size_ << std::endl;
    }

    virtual void read(std::istream& in)
    {
        Point pt;
        unsigned int size;

        in >> pt >> size;

        set_point(pt);
        set_size(size);
    }

    virtual void write(std::ostream& out)
    {
        out << "Circle " << point() << " " << size() << std::endl;
    }
};

}


#endif /* SQUARE_HPP_ */
