#include "square.hpp"
#include "shape_factory.hpp"

namespace
{
    bool is_registered =
            Drawing::ShapeFactory::instance()
                .register_creator("Square", Drawing::ShapeCreator<Drawing::Square>());
}

