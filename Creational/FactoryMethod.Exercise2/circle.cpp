#include "circle.hpp"
#include "shape_factory.hpp"

namespace
{
    bool is_registered =
            Drawing::ShapeFactory::instance()
                .register_creator("Circle", Drawing::ShapeCreator<Drawing::Circle>());
}
