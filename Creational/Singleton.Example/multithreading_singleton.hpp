#ifndef MULTITHREADING_SINGLETON_HPP
#define MULTITHREADING_SINGLETON_HPP

#include <mutex>
#include <memory>

namespace Multithreading
{
	template<typename T>
    class SingletonHolder
	{
	private:
        SingletonHolder() = default;
        static std::unique_ptr<T> instance_;
        static std::once_flag init_flag_;
		static void init_instance()
		{
            instance_.reset(new T());
		}
	public:
        SingletonHolder(const SingletonHolder&) = delete;
        SingletonHolder<T>& operator=(const SingletonHolder&) = delete;

		static T& instance()
		{
            std::call_once(init_flag_, [] { instance_.reset(new T()); });
			return *instance_;
		}
	};

	template<typename T>
    std::unique_ptr<T> SingletonHolder<T>::instance_;

	template<typename T>	
    std::once_flag SingletonHolder<T>::init_flag_;
}

#endif
