#ifndef TEMPLATE_METHOD_HPP_
#define TEMPLATE_METHOD_HPP_

#include <iostream>
#include <string>
#include <memory>

class Service
{
public:
    virtual void run()
    {
        std::cout << "Service::run()" << std::endl;
    }

    virtual ~Service() = default;
};

class BetterService : public Service
{
public:
    virtual void run()
    {
        std::cout << "BetterService::run()" << std::endl;
    }
};

// "AbstractClass"
class AbstractClass
{
protected:
	virtual void primitive_operation_1() = 0;
	virtual void primitive_operation_2() = 0;
    virtual std::unique_ptr<Service> create_service()
    {
        return std::unique_ptr<Service>{new Service()};
    }

    virtual bool is_valid() const
    {
        return true;
    }

    virtual void log(const std::string& msg)
    {}
public:
	void template_method()
	{
		primitive_operation_1();
        if (is_valid())
            primitive_operation_2();

        auto srv = create_service();
        srv->run();

        log("End of work");
	}
	
	virtual ~AbstractClass() {}
};

// "ConcreteClass"
class ConcreteClassA : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation2()" << std::endl;
	}

    bool is_valid() const
    {
        return false;
    }
};

// "ConcreteClass"
class ConcreteClassB : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation2()" << std::endl;
	}

    std::unique_ptr<Service> create_service()
    {
        return std::unique_ptr<Service>{ new BetterService() };
    }

    void log(const std::string& msg)
    {
        std::cout << "Log: " << msg << std::endl;
    }
};

#endif /*TEMPLATE_METHOD_HPP_*/
